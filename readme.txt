When used in conjuction with MathType, allows for the export of multiline equations formatted for amsmath align* environment.

To use these files, you must have MathType already installed on your machine.  Copy these files into the translator files directory 
(in Windows, c:\Program Files\MathType\Translators).  Once installed, you should see a AMSAlignLaTeX listed in MathType under
Preferences -> Cut and Copy preferences -> MathML or TeX.

Use MathType to enter your multiline equation.  IMPORTANT: To mark the proper location for aligment, position the cursor at the proper alignment location
on each line and use the CTRL+; shortcut.  Alternatively, if you prefer to use the GUI instead of keyboard shortcuts, 
you can use the second button on the top row: after clicking on this button, select the leftmost icon on the top row 
(a dotted vertical line with a small triangleat the bottom).

The \intertext{ } command can be invoked by formatting the desired text with the User2 Style (Menu: Style -> User2).

If you want to invoke \boldsymbol for Greek characters (for example, to denote vectors or matrices), format the desired characters with the User1 style.
This avoids the issue that Greek characters are not bolded when using the Vector-Matrix style.  If you make the style of Greek characters Vector-Matrix,
the translator will encode them as \mathbf{ } which will NOT show bold for non-alphabetics.  You can set the User 1 format to display as bold symbol in MathTyple by using
Style -> Define -> Advanced.

When a multiline equation is copied and pasted into LaTeX, it should compile properly in LaTeX without any modification.